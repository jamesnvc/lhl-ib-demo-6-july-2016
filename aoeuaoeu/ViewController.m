//
//  ViewController.m
//  aoeuaoeu
//
//  Created by James Cash on 06-07-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (IBAction)stuff:(id)sender {
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.view.layer.cornerRadius = 20.0;
//    self.view.backgroundColor = [UIColor blueColor];
    UIView *xibView = [[[NSBundle mainBundle] loadNibNamed:@"View" owner:self options:@{}] firstObject];
//    [self.view addSubview:xibView];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    NSLog(@"Should we segue %@", identifier);
    if ([identifier isEqualToString:@"ourSegue"] && YES) {
        return YES;
    }
    return NO;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"Getting ready to segue");
    UIViewController *dest = segue.destinationViewController;
    dest.view.backgroundColor = [UIColor orangeColor];
}

@end
